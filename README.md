 **《练习作品-坦克大战》** 

这个程序是用JAVA开发的，所以你操作系统需要安装好JAVA环境，

如果没有安装JAVA环境则请下载[jre8_86.zip](http://pan.baidu.com/s/1eSLs1Ku)，解压安装jar8_86.exe，

按指示安装完成后，双击运行：[ZiryTankWarV1.2.jar](https://git.oschina.net/zirylee/ZiryTankWar/attach_files)

![输入图片说明](http://git.oschina.net/uploads/images/2016/1123/094937_0b7e5c1e_589425.png "在这里输入图片标题")